#include "display.hpp"

auto chip8_display::clear_display(display_t &display) -> void {
  for (auto &row : display) {
    for (auto &pixel : row) {
      pixel = false;
    }
  }
}