
#ifndef CHIP8_DISPLAY_HPP
#define CHIP8_DISPLAY_HPP
#include <array>
#include <cstdint>
class chip8_display {
  constexpr static std::uint8_t SCREEN_WIDTH = 64;
  constexpr static std::uint8_t SCREEN_HEIGHT = 32;
  using display_t = std::array<std::array<bool, SCREEN_WIDTH>, SCREEN_HEIGHT>;
  constexpr static std::uint8_t SPRITE_SIZE = 5;
  constexpr static std::uint8_t SPRITE_WIDTH = 8;

public:
  display_t m_display{};
  static auto clear_display(display_t &display) -> void;
};

#endif // CHIP8_DISPLAY_HPP
