#include "file_handler.hpp"
#include <array>
#include <cstdint>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <utility>

file_handler::file_handler(std::filesystem::path path)
    : m_path(std::move(path)) {
  if (std::filesystem::exists(m_path)) {
    m_file = std::ifstream(m_path, std::ios::in);
    if (!m_file.is_open()) {
      std::cout << " could not open file" << '\n';
    }
    m_stream << m_file.rdbuf();
  } else {
    std::cout << "file does not exist" << '\n';
  }
}
auto file_handler::load_hex_values()
    -> std::array<std::byte, SPRITE_ARRAY_SIZE> {
  std::string line;
  std::array<std::byte, SPRITE_ARRAY_SIZE> chars{};
  int iter = 0;
  while (std::getline(m_stream, line)) {
    if (line.empty()) {
      continue;
    }
    chars.at(iter) =
        std::byte{static_cast<std::uint8_t>(std::stoi(line, nullptr, 16))};
  }
  ++iter;

  return chars;
};
