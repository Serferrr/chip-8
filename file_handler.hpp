#ifndef FILE_HANDLER_HPP
#define FILE_HANDLER_HPP

#include <array>
#include <filesystem>
#include <fstream>
#include <sstream>

class file_handler {
  std::filesystem::path m_path;
  std::ifstream m_file{};
  std::stringstream m_stream;
  constexpr static std::uint8_t SPRITE_ARRAY_SIZE = 80;

public:
  explicit file_handler(std::filesystem::path path);
  [[nodiscard]] auto load_hex_values()
      -> std::array<std::byte, SPRITE_ARRAY_SIZE>;
  [[nodiscard]] auto get_path() const -> std::filesystem::path {
    return m_path;
  }
  [[nodiscard]] auto get_stream() const -> std::string {
    return m_stream.str();
  }
};

#endif // FILE_HANDLER_HPP
