
#ifndef CHIP8_INSTRUCTIONS_HPP
#define CHIP8_INSTRUCTIONS_HPP

struct instructions final {

  auto CLS(Chip8Display &display) -> void {
    display.clear_display(display.display);
  }
};

#endif // CHIP8_INSTRUCTIONS_HPP
