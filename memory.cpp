#include "memory.hpp"
#include <algorithm>
#include <array>
#include <ranges>

auto Chip8Memory::load_hex_values(std::array<std::byte, 80> chars) -> void {

  std::ranges::copy(chars.begin(), chars.end(), m_ram.begin());
}