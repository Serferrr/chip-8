#ifndef CHIP8_MEMORY_HPP
#define CHIP8_MEMORY_HPP

#include <array>
#include <cstdint>
#include <functional>
#include <stack>

class Chip8Memory final {
public:
  constexpr static std::uint16_t RAM_SIZE = 4096;
  constexpr static std::uint16_t PROGRAM_START = 0x200;
  constexpr static std::uint16_t PROGRAM_END = 0xFFF;
  constexpr static std::uint16_t FONT_END = 0x80;
  constexpr static std::uint8_t REGISTER_SIZE = 16;
  constexpr static std::uint8_t STACK_SIZE = 16;
  constexpr static std::uint8_t SPRITE_ARRAY_SIZE = 80;
  auto load_hex_values(std::array<std::byte, SPRITE_ARRAY_SIZE> chars) -> void;
  auto get_ram() -> std::array<std::byte, RAM_SIZE> & { return m_ram; }

private:
  std::array<std::byte, RAM_SIZE> m_ram{};
  std::array<std::byte, STACK_SIZE> m_registers{};
  std::uint16_t m_memory_address{};
  std::byte m_delay_timer{};
  std::byte m_sound_timer{};
  std::stack<std::function<void()>> m_stack;
  std::uint8_t m_stack_pointer{0};
};
#endif // CHIP8_MEMORY_HPP
