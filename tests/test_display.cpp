#include "display.hpp"
#include <gtest/gtest.h>

TEST(Chip8DisplayTest, ClearDisplay) {
  chip8_display display{};
  display.m_display[0][0] = true;
  display.m_display[10][20] = true;
  display.m_display[31][63] = true;
  ASSERT_TRUE(display.m_display[0][0]);
  ASSERT_TRUE(display.m_display[10][20]);
  ASSERT_TRUE(display.m_display[31][63]);
  chip8_display::clear_display(display.m_display);
  for (auto const &row : display.m_display) {
    for (auto const &pixel : row) {
      ASSERT_FALSE(pixel);
    }
  }
}

auto main(int argc, char **argv) -> int {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}