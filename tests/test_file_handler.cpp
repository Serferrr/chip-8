#include "file_handler.hpp"
#include <gtest/gtest.h>

TEST(FileHandlerTest, NonExistentFile) {
  file_handler const handler("/path/to/nonexistent/file");
  // Ensure that the file does not exist
  ASSERT_FALSE(std::filesystem::exists(handler.get_path()));
}

TEST(FileHandlerTest, ExistingFile) {
  file_handler const handler("../tests/test_text_file.txt");
  // Ensure that the file exists
  ASSERT_TRUE(std::filesystem::exists(handler.get_path()));
  // Ensure that the file contents were read correctly
  ASSERT_EQ(handler.get_stream(), "file contents");
}

auto main(int argc, char **argv) -> int {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}