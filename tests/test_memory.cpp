#include "memory.hpp"
#include <algorithm>
#include <array>
#include <cstddef>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <ranges>

constexpr inline std::size_t PROGRAM_START = 0;
TEST(Chip8MemoryTest, LoadEmptyValues) {
  Chip8Memory memory;
  std::array<std::byte, Chip8Memory::SPRITE_ARRAY_SIZE> chars{};

  // Test loading all zeros
  memory.load_hex_values(chars);
  auto const &ram = memory.get_ram();
  for (auto byte : ram) {
    EXPECT_EQ(byte, std::byte{0});
  }
}

TEST(Chip8MemoryTest, LoadfullValues) {
  // Test loading all full
  Chip8Memory memory;
  std::array<std::byte, Chip8Memory::SPRITE_ARRAY_SIZE> chars{};
  std::ranges::fill(chars.begin(), chars.end(), std::byte{0xFF});
  memory.load_hex_values(chars);
  auto ram = memory.get_ram();
  for (std::size_t i = 0; i < Chip8Memory::SPRITE_ARRAY_SIZE; ++i) {
    EXPECT_EQ(ram[i], std::byte(0xFF));
  }
}

TEST(Chip8MemoryTest, LoadPatternValues) {
  // Test loading a pattern
  Chip8Memory memory;
  std::array<std::byte, Chip8Memory::SPRITE_ARRAY_SIZE> chars{};
  chars[0] = std::byte{0xF0};
  chars[1] = std::byte{0x90};
  chars[2] = std::byte{0x90};
  chars[3] = std::byte{0x90};
  chars[4] = std::byte{0xF0};
  memory.load_hex_values(chars);
  auto const &ram = memory.get_ram();
  EXPECT_EQ(ram[PROGRAM_START], std::byte{0xF0});
  EXPECT_EQ(ram[PROGRAM_START + 1], std::byte{0x90});
  EXPECT_EQ(ram[PROGRAM_START + 2], std::byte{0x90});
  EXPECT_EQ(ram[PROGRAM_START + 3], std::byte{0x90});
  EXPECT_EQ(ram[PROGRAM_START + 4], std::byte{0xF0});
}

auto main(int argc, char **argv) -> int {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}